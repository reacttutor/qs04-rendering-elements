# Rendering Elements

> [Quick Start: Rendering Elements](https://reactjs.org/docs/rendering-elements.html)



    create-react-app rendering-elements --scripts-version=react-scripts-ts
    cd rendering-elements


Remove all "App" files under `/src` and update the `index.tsx`:

	function tick() {
	  const element = (
		<div>
		  <h1>Hello, world!</h1>
		  <h2>It is {new Date().toLocaleTimeString()}.</h2>
		</div>
	  );
	  ReactDOM.render(
		element,
		document.getElementById('root')
	  );
	}

	setInterval(tick, 1000);


Then, run a dev server:

    npm start




